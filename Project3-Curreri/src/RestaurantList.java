import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class RestaurantList{
	int i;
	//Creates Array List of Restaurants
ArrayList<Restaurant> RestList = new ArrayList<Restaurant>(); 

	//List used when searching
ArrayList<Restaurant> found = new ArrayList<Restaurant>();

	private Scanner x; 

	

	public void openFile(){
		try{
			x = new Scanner(new File ("Restaurants.txt")); 
			
		}
		catch(Exception e){
			System.out.println("could not find file");
		}
	}
	
	//Loads Restaurants from Restaurants.txt
	public void LoadRestaurants(){
		String Name;
		while(x.hasNext()){
			
			String line = x.nextLine();
			String[] linea = line.split(",");
		
		String ID = linea[0]; 
		Name = linea[1].toLowerCase();
		String Cuisine =linea[2].toLowerCase();
		String street= linea[3].toLowerCase();
		String city =linea[4].toLowerCase();
		String state =linea[5].toLowerCase();
		String zip = linea[6]; 
		
	    String  Liquor = linea[7]; 
    	boolean liq; 
	    if(Liquor.equalsIgnoreCase("T")){
	    	liq = true; 
	    }
	    else {
	    	liq = false;
	    }
		String number = linea[8]; 
		int numberi = Integer.parseInt(number);
		
		ReservationList tempList = new ReservationList();
			
		Restaurant a = new Restaurant(ID, Name, Cuisine, street, city, state, zip,
				liq, numberi, tempList);
		RestList.add(a); 
	
	}	
		}
		public Restaurant ThisRestaurant(String ID) {
				for (Restaurant Temp : RestList){
					if (Temp.getID().equals(ID)){
						return Temp;
		}			
		}
				return null;
		}
		
		
	public void closeFile(){
		x.close();
	}
	public void add(Restaurant a) {
		RestList.add(a);
	}
	
	public Restaurant GetRest(int i){
		return RestList.get(i);
	}

	public String getName(int index){
		String namee = RestList.get(index).getName(); 
		return namee; 
}
	
	public String getCuisine(int index){
		String Cuisinee = RestList.get(index).getCuisine(); 
		return Cuisinee; 
	}
	
	public String getCity(int index){
		String Cityy = RestList.get(index).getCity();
		return Cityy; 
	}
	
	public String getState(int index){
		String Statee = RestList.get(index).getCity();
		return Statee;
	}
	
	public String getID(int index){
		String IDD = RestList.get(index).getID();
		return IDD; 
	}
	
	public Restaurant getbyID(String ID){
	for (Restaurant a : RestList) {
		if (a.getID().equalsIgnoreCase(ID))
			return a;
	}
			return null; 
	}
	
	
	public void SearchbyName(String name) {
		found.clear();
		int count = 0;
			for (Restaurant Temp : RestList){
				if (Temp.getName().startsWith(name)){
					count++;
					found.add(Temp);
					System.out.println("#" + count);
					Temp.display();
	}
	}
	}

	public void SearchbyCuisine(String name) {
		found.clear();
		int count = 0;
			for (Restaurant Temp : RestList){
				if (Temp.getCuisine().startsWith(name)){
					count++;
					found.add(Temp);
					System.out.println("#" + count);
					Temp.display();
	}
	}
	}

	public void SearchbyCity(String name) {
		found.clear();
		int count = 0;
			for (Restaurant Temp : RestList){
				if (Temp.getCity().startsWith(name)){
					count++;
					found.add(Temp);
					System.out.println("#" + count);
					Temp.display();
	}
	}
	}
	public void SearchbyState(String name) {
		found.clear();
		int count = 0;
			for (Restaurant Temp : RestList){
				if (Temp.getState().startsWith(name)){
					count++;
					found.add(Temp);
					System.out.println("#" + count);
					Temp.display();
	}
	}
	}
	public void SearchbyID(String name) {
		found.clear();
		int count = 0;
			for (Restaurant Temp : RestList){
				if (Temp.getID().startsWith(name)){
					count++;
					found.add(Temp);
					System.out.println("#" + count);
					Temp.display();
	}
	}
	}
	public void display() {
		for(Restaurant temp : found) {
			
			System.out.println("Restaurant ID: " + temp.getID());
			System.out.println();
			System.out.println("Cuisine: " + temp.getCuisine() + "         " + "Liquor License: " + temp.getLiq());
			System.out.println("Address");
			System.out.println(temp.getState()+ "   ,   " + temp.getCity() + "  ,  " + temp.getState() + "  ,  " + temp.getZip());
			System.out.println("________________________________________________________");
			System.out.println();
		}
		}
	
	
		}
	

