import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UserInterface {
	private Scanner in; 
	private ReservationSystem rsystem;
	User ThisUser = null; 

//List used to store found Restaurants when Searching
	ArrayList<Restaurant> found = new ArrayList<Restaurant>();

	   public UserInterface(ReservationSystem system) {
	      in = new Scanner(System.in);
	      rsystem = system;
	   }
	
	
	public void run(){
	
		
		LocalDateTime now = LocalDateTime.now();
		
		ReviewList reviews = new ReviewList();
	
		int response;
	
		// Load users and restaurants
		RestaurantList List = new RestaurantList();
		ReservationList resList = new ReservationList();
		UserList UList = new UserList();
		
		UList.openFile();
		UList.loadFile();
	
		
		List.openFile();
		List.LoadRestaurants();
		
		System.out.println("**Log In**");
		
		ThisUser = UList.Login();
		
// Start of UI
	
		while(ThisUser == null) {
		   try { 
		System.out.println("Welcome " + ThisUser.getname() + " Please enter a number!");
		   }
		   catch(Exception NullPointerException) {
			   System.out.println("User Not Found");
			   ThisUser = UList.Login();
		   }
		}
		int looper = 0;
	
		if(ThisUser!=null) {
			   System.out.println("Welcome " + ThisUser.getname() + " Please enter a number!"); 
		   }
		
	do {
		System.out.println("_____________________________");
		System.out.println("1. Search for restaurant");
		System.out.println("2. Display Info");
		System.out.println("3. Add a review");
		System.out.println("4. My reviews");
		System.out.println("5. Place reservation");
		System.out.println("6. Options");

		response = in.nextInt();
		
// #1 SEARCH and PRINT
		
		if (response == 1){
			System.out.println("**SEARCH**");
			System.out.println("How would you like to search?");
			System.out.println("1. Name");
			System.out.println("2. Cuisine");
			System.out.println("3. city");
			System.out.println("4. State");
			System.out.println("5. Back");
			
			int res; 
			res = in.nextInt();
			
		
//-- Searching for a restaurant by its name
			
			if (res == 1){ System.out.println("Search by name:  ");
			String input = in.next();
		List.SearchbyName(input);
		
			}
			
// --Cuisine search
			
			else if (res == 2){ System.out.println("Search by Cuisine: ");
			String input = in.next();
		List.SearchbyCuisine(input);
			
			}
// --City search
			else if (res == 3){ System.out.println("Search by City: ");
			String input = in.next();
		List.SearchbyCity(input);
			}		

//--State Search
			else if (res == 4){ System.out.println("Serach by State: ");
			String input = in.next();
		List.SearchbyState(input);
			
		}
		
		}
// #2 DISPLAY	
		//Displays restaurant info by ID or NAME
		if (response == 2){
			System.out.println("**RESTAURANT INFO**");
			System.out.println();
			System.out.println("Enter Restaurant ID: ");
			System.out.println("1. Back");
				
		}
//#3 add a review
		else if (response ==3){
			
			
			String UserID = ThisUser.getUsername();
			
			reviews.add(UserID);
			
		}
//#4 show user reviews
		else if(response == 4){
			System.out.println("**POSTED REVIEWS**");
				
				reviews.display();
			
}
//#5 Make Reservation		
		else if(response == 5){
				ReservationSystem RS = new ReservationSystem(UList, List, resList, reviews);
				RS.run();
		}	
		else if(response == 6) {
		
		System.out.println("Would you like to Quit?");
		int ans;
		System.out.println("1. Yes");
		System.out.println("2. Bring me to Restaurant Interface");
		System.out.println("3.No, back to the top!");
		ans=in.nextInt();
		
		if (ans == 1){
			looper=1;
			in.close();
			System.out.println("Good-Bye!");
		}
		if (ans == 2) {
			looper=1;
			
			RestaurantInterface Switch = new RestaurantInterface(rsystem);
			Switch.run();
			
		}
		}
		
	}while(looper != 1);

	
	

	
}
}








