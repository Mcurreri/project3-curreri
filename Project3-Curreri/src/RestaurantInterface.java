import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RestaurantInterface {
	
	
	
	 public RestaurantInterface(ReservationSystem system)
	   { }


		

// Method that starts the Restaurant Interface
public void run(){
	
	// Load infortmation
	RestaurantList List = new RestaurantList();
	List.openFile();
	List.LoadRestaurants();
	
	
	ReviewList reviews = new ReviewList();
	reviews.loadreviews();
	
	Scanner in = new Scanner(System.in);
	
	// Starts by accepting a Restaurant Id
	    System.out.println("Enter Restaurant ID");
	    System.out.println("(some test numbers -41576328--50058705--41243427- )");
	    String ID = in.next();
		Restaurant ThisRestaurant = List.ThisRestaurant(ID);
 // this line searches and prints the restaurant identified by the entered ID
		System.out.println("Welcome admin of " + List.getbyID(ID).getName());
		
		int response=0; 
	
		do{
		System.out.println("____________________________");
		System.out.println("Please choose from an option below");
		System.out.println("1. Show reviews");
		System.out.println("2. Statistics");
		System.out.println("3. Show Reservations");
		System.out.println("4. Quit");
		response = in.nextInt();
		
		
			
// Shows the reviews of the found restaurant 	
		if (response ==1){
		System.out.println("Reviews of " + List.getbyID(ID).getName() + " (" + List.getbyID(ID).getStreet() + ")");
		
		if(reviews.size() == 0){
			System.out.println("--NO REVIEWS--");
		}
		}
	
	//Displays restaurants statistics
		 if (response==2){
			System.out.println("Restaurant's Statitistics");
			reviews.getStats();
		
		 }
		 
		 if (response==3) {
			 System.out.println("Restaurant Reservations");
			 ThisRestaurant.displayRes();
			 }
		 
		 
		}while(response != 4);
			
		System.out.println("Would you like to switch to the User InterFace?");
		int res = 0;
		System.out.println("1.Yes");
		System.out.println("2.no");
		res=in.nextInt();
		
		if(res==1){
			
			UserInterface transition = new UserInterface(null);
			transition.run(); 
		}
		else{
			System.out.println("Good-Bye!");
		}
		
		
}


}
			
		


		
	
		


