import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Scanner;

// This Class manages the relationships of Users and Restaurants 
// on Reservations and Reviews

public class ReservationSystem {
	
	Scanner in = new Scanner(System.in);


	
	private UserList userList;
	private ReservationList reservations;
	private RestaurantList restList;
    private ReviewList reviews;
    private String currentId;                     
    
    
public ReservationSystem(UserList users, RestaurantList restList, ReservationList reservations,
			ReviewList reviews) {
	this.userList = users;
	this.reservations = reservations;
	this.reviews = reviews;
	this.restList = restList; 
	
	restList.openFile();
	restList.LoadRestaurants();
	
	}

public boolean isEmpty() {
	if(reservations.isEmpty())
		return true;
	else
		return false;

	
}
public Boolean loginUser(String userId) {
	User user = userList.find(userId);
	 if (user != null) {
	    	currentId = userId;
	    	return true;
	    }
	return false;
}
public void run() {
	int response1= 0;
	
	System.out.println("Welcome to the reservation system!");
	
	System.out.println("Please enter UserId: ");
	String Username = in.next();
	
	User user = userList.find(Username);
	System.out.println("Choose below");
	

do {
	
	System.out.println("1. Make Reservation");
	System.out.println("2.Check existing Reservations");
	System.out.println("3. Quit");
	 
			response1 = in.nextInt();
			
	if(response1 ==1){
		
	System.out.println("what is the name of the restaurant?");
		String name2 = in.next();
		
		
	System.out.println("Results for '" + name2 + "'");
		restList.SearchbyName(name2);
		
	System.out.println("Please enter the ID of the Restaurant you wish to reserve");
		String input = in.next();
		
		Restaurant Restaurant = restList.getbyID(input);
		
		
		Restaurant target = restList.getbyID(input);
		
		String Tname = target.getName();
		
		System.out.println("What month (in number form) will your reservation be in " + Tname + "?");
		int month = in.nextInt();
		
		System.out.println("On what day will you be eating here?");
		int day = in.nextInt();

		LocalDate time = LocalDate.of(2018, month , day);
		Reservation a = new Reservation("JOE", Tname, time);
		
		
		System.out.println();
		a.display();
		System.out.println();
		;
		
		
		// Add new reservation to both Lists 
		
		user.addRestoList(a);
		Restaurant.addRes(a);
		
		
	}
	
	else {
		for(int i=0; i<user.Reservations.size(); i++)
		user.GetUserRes(i).display();
		
	}


}while(response1 !=3);
}
}
