
//PROJECT 1 DRIVER 
//Matthew Curreri 
//CUS1156 
//3/12/2018

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Scanner;


public class SystemStartup {
	
	
	public static void main(String[] args) {
	    UserInterface ui;
		RestaurantInterface ri;
	    UserList users = loadUsers();
	    RestaurantList restaurants = loadRestaurants();
	    ReviewList reviews = loadReviews();
	    ReservationList reservations = new ReservationList();
	    Scanner in = new Scanner(System.in);
	    
	
        
	    ReservationSystem resSystem = new ReservationSystem(users, restaurants, reservations, reviews);
	    
	    //Log in
	    
	    System.out.println("What type of user are you? Type user or restaurant");
	   
	   String type = in.nextLine();
	   
	   
	    if (type.equalsIgnoreCase("user")){
	    	
	        ui = new UserInterface(resSystem);
	        ui.run();
	    
	    }
	    else{
	    	ri = new RestaurantInterface(resSystem);
	        ri.run();
	    }
        writeReviews(reviews);
        in.close();
	}
	
	private static ReviewList loadReviews() {
		ReviewList revs = new ReviewList();
		Scanner fileIn;
	    try {
			fileIn = new Scanner(new File("reviews.txt"));
		
		
		while (fileIn.hasNextLine())
	    {
		 
		 String line = fileIn.nextLine();
		 
		 review rev = processReviewInputLine(line);
		 
		 revs.add(rev);
		
		 
   	}
		fileIn.close();
	    } catch (FileNotFoundException e) {
			System.err.println("Error reading rental file");
			e.printStackTrace();
		}
		
		return revs;
	}

	private static review processReviewInputLine(String line) {
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(",");
		String uid = lineScanner.next();

		String rid = lineScanner.next();
		String revTxt = lineScanner.next();
		int numStars = lineScanner.nextInt();
		String dateStr = lineScanner.next();

	    LocalDateTime dateOfReview = LocalDateTime.now();
		 try {
				DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
				dateOfReview = LocalDateTime.parse(dateStr, formatter);
				
			} catch (DateTimeParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 
		 review rev = new review(revTxt, numStars,uid, rid, dateOfReview);
		 return rev;
	}

	private static void writeReviews(ReviewList reviews) {
		FileWriter outFile;
		
		try {
			outFile = new FileWriter(new File("reviews.txt"));
			// for debugging purposes
			// System.err.println(rentals.toString());
			 List<review> export = reviews.export();
			 String newline = System.getProperty("line.separator");
			 for (review curr : export)
				 outFile.write(curr.getUser() + "," + curr.getrid() + "," + curr.getname() 
				    + "," + curr.getStars() + "," + curr.getTime() + newline);
			 
			 outFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private static RestaurantList loadRestaurants() {
		RestaurantList restList = new RestaurantList();
		Scanner fileIn;
	    try {
			fileIn = new Scanner(new File("restaurants.txt"));
		
		while (fileIn.hasNextLine())
	    {
		 
		 String line = fileIn.nextLine();
		 
		 Restaurant rest = processRestInputLine(line);
		// System.out.println("restaurant id " + rest.getID());
		 restList.add(rest);
   	}
		fileIn.close();
	    } catch (FileNotFoundException e) {

			 System.err.println("Error reading restaurant file");
			e.printStackTrace();
		}
		
		return restList;
	}

	private static Restaurant processRestInputLine(String line) {
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(",");
		String id = lineScanner.next();
		String name = lineScanner.next();
		String cuisine = lineScanner.next();
		String addr1 = lineScanner.next();
		String city = lineScanner.next();
		String state = lineScanner.next();
		String zip = lineScanner.next();
		char liqFlag = lineScanner.next().charAt(0);
		Boolean hasLic;
		if (liqFlag == 'T')
			hasLic=true;
		else
			hasLic = false;
		int capacity = lineScanner.nextInt();
		
	ReviewList review = new ReviewList();
     review = review.WriteReviews(loadReviews());
	  
     ReservationList reservations = new ReservationList();
		Restaurant rest = new Restaurant(id,name, cuisine, addr1,city,state,zip, hasLic, capacity, reservations);
		 		 
		 lineScanner.close();
		return rest;
	}
	
	

	private static User processUserInputLine(String line) {
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter(",");
		String id = lineScanner.next();
		String fname = lineScanner.next();	 
		String lname = lineScanner.next();

		 ReservationList Reviews = new ReservationList();
		
	    User user = new User(id,fname,lname, Reviews);
		 		 
		 lineScanner.close();
		return user;
	}
	
	private static UserList loadUsers() {
		UserList userList = new UserList();
		Scanner fileIn;
	    try {
			fileIn = new Scanner(new File("users.txt"));
		
		
		while (fileIn.hasNextLine())
	    {
		 
		 String line = fileIn.nextLine();
		 
		 User user = processUserInputLine(line);
		 
		 userList.add(user);
   	}
		fileIn.close();
	    } catch (FileNotFoundException e) {
		    System.err.println("error reading user file");
			e.printStackTrace();
		}
		
		return userList;
	}


	}


