// This class creates the Restaurant object
public class Restaurant {

	private String name; 
	private String cuisine; 
	private String id;
	private boolean hasLic; 
	private String addr; 
	private String city; 
	private String state; 
	private String zip;
	int capacity; 
	ReviewList reviews;
	ReservationList Reservations = new ReservationList();
	
	//This method creates the restaurant object

	

	public Restaurant(String id, String name, String cuisine, String addr, String city, String state, String zip,
			Boolean hasLic, int capacity, ReservationList Reservations) {

		this.name= name; 
		this.cuisine = cuisine; 
		this.id= id; 
		this.hasLic = hasLic;  
		this.addr = addr; 
		this.city= city; 
		this.state=state; 
		this.zip=zip; 
		this.capacity = capacity; 
		this.Reservations=Reservations; 
	
	}


	
	
	public void addReveiw(review Review) {
		reviews.add(Review);
	}
	public void addRes(Reservation ReservationTOadd) {
		Reservations.addRes(ReservationTOadd);
	}
	public Reservation getRes(int index) {
		return Reservations.GetRes(index);
	
	}

	public String getID(){
		return id;
	}
	public String getName(){
	return name; 
	}
	public String getCuisine(){
		return cuisine; 
	}
	public String getCity(){
		return city;
	}
	public String getState(){
		return state;
	}
	public String getStreet(){
		return addr;
	}
	public boolean getLiq() {
		return hasLic;
	}
	public String getZip(){
		return zip;
	}
	public void display(){
			
		System.out.println("**" + name + "**");
		System.out.println("Restaurant ID: " + id);
		System.out.println();
		System.out.println("Cuisine: " + cuisine + "         " + "Liquor License: " + hasLic);
		System.out.println("Address");
		System.out.println(addr + "   ,   " + city + "  ,  " + state + "  ,  " + zip);
		System.out.println("________________________________________________________");
		System.out.println();
		
		
	}
	public void displayRes() {
		Reservations.display();
	}
	 
	
		
	}


