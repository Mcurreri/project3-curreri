import java.time.LocalDate;


//this class creates a reservation object to be stored into the reservation list

public class Reservation {

	String user; 
	String restaurant; 
	LocalDate Time; 
	
	public Reservation(String user , String restaurant, LocalDate Time){
		this.user=user;
		this.restaurant=restaurant; 
		this.Time=Time; 
	}
	
	
	public LocalDate getTime(){
		return Time;
	}
	
public void display() {
	System.out.println("Reservation for: " + user + " for " + Time + " at " + restaurant);
	
}


public String getUser() {

	return user;
}


public String getRest() {
	
	return restaurant;
}

}
