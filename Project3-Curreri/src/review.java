
import java.time.LocalDateTime;
// this class creates a review object to store into the review list
public class review {
	


 
 String uid;
 String revTxt; 
 String rid;
 LocalDateTime now = LocalDateTime.now(); 
 int numStars; 
 
 // this  method creates the review
 public review (String revTxt, int numStars, String uid, String rid, LocalDateTime now){
	 this.revTxt=revTxt;
	 this.uid=uid;
	 this.now = now; 
	 this.numStars=numStars; 
	 this.rid=rid; 
 }
 
 
public String getrid(){
	return rid; 
	
}
public LocalDateTime getTime() {
	return now;
}

public String getname() {
	return uid;
}

public int getStars(){
	
	return numStars;
	
}

public String getUser() {
	
	return uid;
}


public LocalDateTime TimePosted() {
	return now;
	
}

// Gets Review Text
public String RevText() {
	
	return revTxt;
}


public void display(){; 
		
			System.out.println("Review from " + uid + " for " + rid);
			System.out.println("''" + revTxt + "''");
			System.out.println("Stars Given : " + numStars);
			System.out.println("Posted at " + now);
		 }

	}

