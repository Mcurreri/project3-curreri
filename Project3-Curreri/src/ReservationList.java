import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.util.ArrayList;


// This Class Holds and Manipulates reservations for both Users andRestaurants. 

public class ReservationList {
	ArrayList<Reservation> Reservations = new ArrayList<Reservation>();
	
	public void addRes(Reservation a){
		Reservations.add(a); 
	}
	
	// Writes/Serializes the Reservation List into txt file
	public void write(String filename) {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(filename));
			os.writeObject(Reservations);
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
			
	}
	
	// Reads the Serialized Text fild for the Date
	public void readbydate() {
		String fileName = "reviews.txt";
		try {
			ObjectInputStream oi = new ObjectInputStream(new FileInputStream(fileName));
			oi.readObject();
			for(Reservation a : Reservations) {
			System.out.println(a.getTime());
			oi.close();
		} }catch (FileNotFoundException e) {
		
		
			e.printStackTrace();
		} catch (IOException e) {
			
		
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		}
	
	
}
	public Reservation GetRes(int index) {
		return Reservations.get(index);
	}
	
	public void display(){
		for(int i=0; i<Reservations.size(); i++){
		String User =	Reservations.get(i).getUser();
		String Restaurant = Reservations.get(i).getRest();
		LocalDate Time = Reservations.get(i).getTime();
		
		System.out.println("________________");
		System.out.println("RESERVATION FOR " + User + "at" + Time);
		System.out.println("      AT     ");
		System.out.println("   " +Restaurant);
		System.out.println("________________");
		System.out.println();		
			
		}
	}
	public boolean isEmpty() {
		if(Reservations.isEmpty()) {
			return true;
		}
		return false;
	}
	public int size() {
		return Reservations.size();
	}
}
