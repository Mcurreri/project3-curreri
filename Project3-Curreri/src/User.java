import java.util.ArrayList;

// This class creates the user object

public class User {
	private String fname; 
	private String lname; 
	private String Username; 
	public ReservationList Reservations = new ReservationList();
	
//this method creates the user
	public User(String Username, String fname, String lname,ReservationList Reservations){
		this.fname = fname; 
		this.lname = lname; 
		this.Username = Username;  
		this.Reservations = Reservations; 
}
	public void addRestoList(Reservation res) {
		Reservations.addRes(res);
	}
	public Reservation GetUserRes(int index) {
		return Reservations.GetRes(index);
	}
// returns the users 'username'
	public String getUsername(){
		return Username;
	}
	public String getname() {
		return fname; 
	}
	public String getLastName() {
		return fname; 
	}
public void display() {
	System.out.println(Username + " " + fname + " " + lname);
}
	
public int numberOfRes() {
	return Reservations.size();
	
}
	}

