import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class UserList   {
	ArrayList<User> UserList = new ArrayList<User>(); 
	
	private Scanner in = new Scanner(System.in);
	private Scanner x;
	public void openFile(){
		try{
			x = new Scanner(new File ("Users.txt")); 
			
		}
		catch(Exception e){
			System.out.println("could not find file");
		}
	}

	public void loadFile() {
		while(x.hasNext()){
			
			String line = x.nextLine();
			String[] linea = line.split(",");
		
		String ID = linea[0]; 
		String FName = linea[1];
		String LName  =linea[2];
	
	   ReservationList ResList = new ReservationList();
	   
	User newUser = new User(ID, FName, LName, ResList);
		UserList.add(newUser); 
	
	}	
		}
		
	
	// List holds all Users
	
	
	public void add(User a){
		UserList.add(a); 
	}
	
	public void closeFile(){
		x.close();
	}
	
	//Gets user from specific index 
	public User getUser(int index){
	return UserList.get(index);
		
	}
	//finds user by ID
	public User getbyID(String ID){
		for (User a : UserList){
		   if(a.getUsername().equals(ID))
		    return a; 
			}
		return null;
	}

	public User find(String userId) {
		User a = null;
		for(User temp: UserList) {
			if(temp.getUsername().contentEquals(userId)) { 
				a = temp;
			}
		}
		return a;
		
	}
public void display() {
	for(User a : UserList) {
		a.display();
	}
}


public User Login() {
		try {
			System.out.println("Enter User ID: ");
			String ID = in.next();
		
			for(User a : UserList) {
		if (ID.equals(a.getUsername())) {
			
		return a;
		}
			}
		}	catch (Exception NullPointerException) {
			
			System.out.println("username not found");
		}
		return null;
		

}
	
}

